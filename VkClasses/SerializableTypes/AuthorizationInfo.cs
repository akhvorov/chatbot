﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace VkClasses.SerializableTypes
{
    [Serializable]
    public class AuthorizationInfo
    {
        /// <summary>
        /// Логин
        /// </summary>
        [XmlElement("Login")]
        public string Login;
        /// <summary>
        /// Пароль
        /// </summary>
        [XmlElement("Password")]
        public string Password;
        /// <summary>
        /// идентификатор пользователя
        /// </summary>
        [XmlElement("UserId")]
        public string UserId;
        /// <summary>
        /// идентификатор приложения
        /// </summary>
        [XmlElement("ApplicationId")]
        public string AppId;
        /// <summary>
        /// Версия Api
        /// </summary>
        [XmlElement("ApiVersion")]
        public string ApiVersion;
        /// <summary>
        /// Токен авторизации
        /// </summary>
        [XmlElement("Token")]
        public string AccessToken;
        /// <summary>
        /// Права доступа для токена
        /// </summary>
        [XmlElement("Scope")]
        public int Scope;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public AuthorizationInfo()
        {
        }
        /// <summary>
        /// Конструктор по логину-паролю
        /// </summary>
        /// <param name="login">номер телефона или e-mail</param>
        /// <param name="password">пароль</param>
        public AuthorizationInfo(string login, string password, string applicationId, string apiVersion = "5.30")
        {
            Login = login;
            Password = password;
            AppId = applicationId;
            ApiVersion = apiVersion;
            AddAllScopes();
            Authorize();
        }

        //TODO: переписать через xNet
        /// <summary>
        /// Метод авторизации текущего пользователя: получение token'а по заданным разрешениям.
        /// </summary>
        public void Authorize()
        {//логинимся на странице
            HttpWebRequest myReq = (HttpWebRequest)HttpWebRequest.Create(
                String.Format("http://api.vkontakte.ru/oauth/authorize?client_id={0}&scope={1}&display=wap&v={2}&response_type=token",
                AppId
                , Scope, ApiVersion));
            MessageBox.Show(myReq.RequestUri.ToString());
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            StreamReader myStream = new StreamReader(myResp.GetResponseStream(), Encoding.UTF8);
            //получили ответ
            string html = myStream.ReadToEnd();

            //получили ответ, ищем форму логина
            Regex myReg = new Regex("<form(.*?)>(?<form_body>.*?)</form>", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
            if (!myReg.IsMatch(html) || (html = myReg.Match(html).Groups["form_body"].Value) == "")
            {
                MessageBox.Show("Не удалось получить форму авторизации. Проверьте шаблон регулярного выражения.");
                return;
            }

            //формируем следующий запрос
            myReg = new Regex("<input(.*?)name=\"(?<name>[^\x22]+)\"(.*?)((value=\"(?<value>[^\x22]*)\"(.*?))|(.?))>", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
            NameValueCollection qs = new NameValueCollection();
            foreach (Match m in myReg.Matches(html))
            {
                string val = m.Groups["value"].Value;
                if (m.Groups["name"].Value == "email")
                {
                    val = Login;
                }
                else if (m.Groups["name"].Value == "pass")
                {
                    val = Password;
                }
                qs.Add(m.Groups["name"].Value, HttpUtility.UrlEncode(val));//может падать
            }

            //отправляем методом post и делаем куку
            byte[] b = System.Text.Encoding.UTF8.GetBytes(String.Join("&", from item in qs.AllKeys select item + "=" + qs[item]));

            myReq = (HttpWebRequest)HttpWebRequest.Create("https://login.vk.com/?act=login&soft=1&utf8=1");
            myReq.CookieContainer = new CookieContainer();
            myReq.Method = "POST";
            myReq.ContentType = "application/x-www-form-urlencoded";
            myReq.ContentLength = b.Length;
            myReq.GetRequestStream().Write(b, 0, b.Length);
            myReq.AllowAutoRedirect = false;

            myResp = (HttpWebResponse)myReq.GetResponse();

            //получаем куку
            CookieContainer cc = new CookieContainer();
            foreach (Cookie c in myResp.Cookies)
            {
                cc.Add(c);
            }

            //разбираемся с разрешениями
            if (!String.IsNullOrEmpty(myResp.Headers["Location"]))
            {
                // делаем редирект
                myReq = (HttpWebRequest)HttpWebRequest.Create(myResp.Headers["Location"]);
                myReq.CookieContainer = cc;// передаем куки
                myReq.Method = "GET";
                myReq.ContentType = "text/html";

                myResp = (HttpWebResponse)myReq.GetResponse();
                myStream = new StreamReader(myResp.GetResponseStream(), Encoding.UTF8);
                html = myStream.ReadToEnd();
            }
            else
            {
                // что-то пошло не так
                MessageBox.Show("Ошибка. Ожидался редирект.");
                return;
            }

            //проверяем залогинились ли мы
            //TODO:разобраться с формой ответа сервера
            /*
            myReg = new Regex("Вы авторизованы как <b><a href=\"(?<url>[^\\x22]+)\">(?<user>[^\\x3c]+)</a></b>", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
            if (!myReg.IsMatch(html))
            {
                MessageBox.Show("!Ошибка: Авторизация не прошла.");
                return;
            }

            MessageBox.Show(String.Format("Авторизация успешно прошла.\nПользователь {0}", myReg.Match(html).Groups["user"].Value));
            */
            if (!html.Contains("выйти"))
            {
                MessageBox.Show("!Ошибка: Авторизация не прошла.");
                MessageBox.Show(html);
                return;
            }
            MessageBox.Show("Форма подтверждения прав доступа получена");

            //получаем форму для авторизации
            myReg = new Regex("<form(.*?)action=\"(?<post_url>[^\\x22]+)\"(.*?)>(?<form_body>.*?)</form>", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
            if (!myReg.IsMatch(html))
            {
                MessageBox.Show("Не удалось получить форму. Проверьте шаблон регулярного выражения.");
                return;
            }

            //если полученный адрес относительный - шаманим
            string url = myReg.Match(html).Groups["post_url"].Value;
            if (!url.ToLower().StartsWith("https://")) { url = String.Format("https://login.vkontakte.ru{0}", url); }

            MessageBox.Show(url);
            //последний запрос
            myReq = (HttpWebRequest)HttpWebRequest.Create(url);
            myReq.CookieContainer = cc; // не забываем передавать куки
            myReq.Method = "POST";
            myReq.ContentType = "application/x-www-form-urlencoded";
            myReq.ContentLength = 0;
            myReq.AllowAutoRedirect = false;

            myResp = (HttpWebResponse)myReq.GetResponse();

            //выдергиваем ключ
            if (!String.IsNullOrEmpty(myResp.Headers["Location"]))
            {
                myReg = new Regex(@"(?<name>[\w\d\x5f]+)=(?<value>[^\x26\s]+)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                foreach (Match m in myReg.Matches(myResp.Headers["Location"]))
                {
                    if (m.Groups["name"].Value == "access_token")
                    {
                        AccessToken = m.Groups["value"].Value;
                    }
                    else if (m.Groups["name"].Value == "user_id")
                    {
                        UserId = m.Groups["value"].Value;
                    }
                    // еще можно запомнить срок жизни access_token - expires_in,
                    // если нужно
                }
                MessageBox.Show(String.Format("Ключ доступа: {0}\nUserID: {1}", AccessToken, UserId));
            }
            else
            {
                MessageBox.Show("Ошибка. Ожидался редирект.");
            }
            MessageBox.Show(ToString());
        }

        /// <summary>
        /// Добавляет все разрешения к авторизации (нужно получать token заново).
        /// </summary>
        public void AddAllScopes()
        {
            Scope =
                (int)
                    (AppInfo.VkontakteScopeList.Ads | AppInfo.VkontakteScopeList.Audio | 
                     AppInfo.VkontakteScopeList.Docs | AppInfo.VkontakteScopeList.Friends | 
                     AppInfo.VkontakteScopeList.Groups | AppInfo.VkontakteScopeList.Link |
                     AppInfo.VkontakteScopeList.Mail | AppInfo.VkontakteScopeList.Messages |
                     AppInfo.VkontakteScopeList.Notes | AppInfo.VkontakteScopeList.Notifications | 
                     AppInfo.VkontakteScopeList.Notify | AppInfo.VkontakteScopeList.Offers | 
                     AppInfo.VkontakteScopeList.Offline | AppInfo.VkontakteScopeList.Pages | 
                     AppInfo.VkontakteScopeList.Photos | AppInfo.VkontakteScopeList.Questions |
                     AppInfo.VkontakteScopeList.Stats | AppInfo.VkontakteScopeList.Status | 
                     AppInfo.VkontakteScopeList.Video | AppInfo.VkontakteScopeList.Wall);
        }

        /// <summary>
        /// Добавление разрешения (нужно переполучить token)
        /// </summary>
        /// <param name="scopeItem">добавляемый ключ</param>
        public void AddScope(AppInfo.VkontakteScopeList scopeItem)
        {
            Scope = Scope | (int)scopeItem;
        }

        public override string ToString()
        {
            return String.Format("Login:{0}\nPassword:{1}\nUserId:{2}\nApplicationId:{3}\nScope:{4}\nToken:{5}\n", Login, Password, UserId, AppId, Scope, AccessToken);
        }
    }
}
