﻿using System;
using System.Collections.Specialized;
using System.Xml.Serialization;

namespace VkClasses.SerializableTypes
{
    /// <summary>
    /// пользователь для получения xml-ответа
    /// </summary>
    [Serializable]
        //TODO: сверить поля
    public class UserInfo
    {
        [XmlElement("id")]
        public int id;
        [XmlElement("first_name")]
        public string FirstName;
        [XmlElement("last_name")]
        public string LastName;
        [XmlElement("sex")]
        public int Sex; //0 - не указано, 1 - женский, 2 - мужской
        [XmlElement("bdate")]
        public string BDate;
        [XmlElement("city")]
        public CityInfo city;
        [XmlElement("online")]
        public int online; //1 - в сети, 0 - не в сети

        string lastMessage;

        public UserInfo()
        {
            id = 0;
            FirstName = "";
            LastName = "";
            Sex = 0;
            BDate = "";
            city = new CityInfo();
            online = 0;
            lastMessage = "";
        }

        public UserInfo(int user_id, string NewUserFile)
        {
            id = user_id;
            var queriesList = new NameValueCollection();
            queriesList["user_ids"] = user_id.ToString();
            queriesList["fields"] = "first_name, last_name, sex, bdate, city, online";
            AppInfo.ExecuteCommand("users.get", queriesList, "").Save(NewUserFile);
        }

        public override string ToString()
        {
            return String.Format("id : {0}\nname : {1} {2}\nsex : {3}\nbirthday : {4}", id, FirstName, LastName, Sex, BDate);
        }
    }
}
