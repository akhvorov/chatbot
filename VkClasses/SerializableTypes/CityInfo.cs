﻿using System;
using System.Xml.Serialization;

namespace VkClasses.SerializableTypes
{
    [Serializable]
    [XmlRoot("city")]//TODO: Написать комментарии, сверить поля.
    public class CityInfo
    {
        [XmlElement("id")]
        int id;
        [XmlElement("title")]
        string title;

        public CityInfo()
        {
            id = 0; 
            title = "";
        }
    }
}
