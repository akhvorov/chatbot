﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using VkClasses.SerializableTypes;

namespace VkClasses
{
    /// <summary>
    /// ответ в виде списка пользователей (для xml-файла)
    /// </summary>
    [Serializable]
    [XmlRoot("response")]
    public class UsersGetResponse<TUserType>
    {
        [XmlElement("user")]
        public List<TUserType> items;

        public UsersGetResponse()
        {
            items = new List<TUserType>();
        }

        public UsersGetResponse(List<TUserType> arr)
        {
            items = arr;
        }
    }
}
