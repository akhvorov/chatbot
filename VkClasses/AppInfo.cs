﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Xml;
using VkClasses.SerializableTypes;

namespace VkClasses
{
    public static class AppInfo
    {
        [Flags]
        public enum VkontakteScopeList
        {
            /// <summary>
            /// Пользователь разрешил отправлять ему уведомления. 
            /// </summary>
            Notify = 1,
            /// <summary>
            /// Доступ к друзьям.
            /// </summary>
            Friends = 2,
            /// <summary>
            /// Доступ к фотографиям. 
            /// </summary>
            Photos = 4,
            /// <summary>
            /// Доступ к аудиозаписям. 
            /// </summary>
            Audio = 8,
            /// <summary>
            /// Доступ к видеозаписям. 
            /// </summary>
            Video = 16,
            /// <summary>
            /// Доступ к предложениям (устаревшие методы). 
            /// </summary>
            Offers = 32,
            /// <summary>
            /// Доступ к вопросам (устаревшие методы). 
            /// </summary>
            Questions = 64,
            /// <summary>
            /// Доступ к wiki-страницам. 
            /// </summary>
            Pages = 128,
            /// <summary>
            /// Добавление ссылки на приложение в меню слева.
            /// </summary>
            Link = 256,
            /// <summary>
            /// Доступ к статусу пользователя. 
            /// </summary>
            Status = 1024,
            /// <summary>
            /// Доступ к заметкам пользователя. 
            /// </summary>
            Notes = 2048,
            /// <summary>
            /// (для Standalone-приложений) Доступ к расширенным методам работы с сообщениями. 
            /// </summary>
            Messages = 4096,
            /// <summary>
            /// Доступ к обычным и расширенным методам работы со стеной. 
            /// </summary>
            Wall = 8192,
            /// <summary>
            /// Доступ к расширенным методам работы с рекламным API.
            /// </summary>
            Ads = 32768,
            /// <summary>
            /// Доступ к API в любое время со стороннего сервера (при использовании этой опции параметр expires_in, возвращаемый вместе с access_token, содержит 0 — токен бессрочный).
            /// </summary>
            Offline = 65536,
            /// <summary>
            /// Доступ к документам пользователя.
            /// </summary>
            Docs = 131072,
            /// <summary>
            /// Доступ к группам пользователя.
            /// </summary>
            Groups = 262144,
            /// <summary>
            /// Доступ к оповещениям об ответах пользователю.
            /// </summary>
            Notifications = 524288,
            /// <summary>
            /// Доступ к статистике групп и приложений пользователя, администратором которых он является.
            /// </summary>
            Stats = 1048576,
            /// <summary>
            /// Доступ к e-mail пользователя.
            /// </summary>
            Mail = 4194304
        }

        public static int Scope;
        public static int AppId;
        public static int UserId;
        public static AuthorizationInfo CurrentAuthorizationInfo;
        public static string AccessToken;
        public static string ApiVersion;
        public static string tempFilesDirectory = @"tempVkApiFiles";

        public static void AddAllScopes()
        {
            Scope =
                (int)
                    (VkontakteScopeList.Ads | VkontakteScopeList.Audio | VkontakteScopeList.Docs |
                     VkontakteScopeList.Friends | VkontakteScopeList.Groups | VkontakteScopeList.Link |
                     VkontakteScopeList.Mail | VkontakteScopeList.Messages |
                     VkontakteScopeList.Notes | VkontakteScopeList.Notifications | VkontakteScopeList.Notify |
                     VkontakteScopeList.Offers | VkontakteScopeList.Offline | VkontakteScopeList.Pages |
                     VkontakteScopeList.Photos | VkontakteScopeList.Questions |
                     VkontakteScopeList.Stats | VkontakteScopeList.Status | VkontakteScopeList.Video |
                     VkontakteScopeList.Wall);
        }

        public static void AddScope(VkontakteScopeList scopeItem)
        {
            Scope = Scope | (int)scopeItem;
        }

        public static string ComposeQuery(string methodName, NameValueCollection queriesList, string accessToken = "") //TODO: сделать прием в формате JSON
        {
            return String.Format("https://api.vk.com/method/{0}.xml?access_token={1}&v={2}&{3}", methodName, ((accessToken == "") ? AccessToken : accessToken), ApiVersion,
                String.Join("&", from item in queriesList.AllKeys select String.Format("{0}={1}", item, queriesList[item])));
        }

        //TODO: выпилить в отдельный класс
        public static XmlDocument ExecuteCommand(string name, NameValueCollection queriesList, string accessToken = "")
        {
            var result = new XmlDocument();
            String query = String.Format("https://api.vk.com/method/{0}.xml?access_token={1}&v={2}&{3}", name, ((accessToken == "") ? AccessToken : accessToken), ApiVersion,
                String.Join("&",
                    from item in queriesList.AllKeys select String.Format("{0}={1}", item, queriesList[item])));
            result.Load(query);
            return result;
        }

        /// <summary>
        /// Выдает путь до случайного файла
        /// </summary>
        /// <param name="folderName">Имя директории в директории приложения</param>
        /// <param name="extention">расширение файла</param>
        /// <param name="nameLength">длина имени</param>
        /// <returns></returns>
        public static string RandomFileName(string extention, string folderName = "", int nameLength = 5)
        {
            var tempDirectory = String.Format(@"{0}/{1}", tempFilesDirectory, folderName);
            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);
            return String.Format(@"{0}\{1}\{2}_{3}.{2}", AppInfo.tempFilesDirectory, folderName, extention,
                RandomString(nameLength));
        }

        //TODO:выпилить в отдельный класс для работы со строками
        private static readonly Random _random = new Random();

        private static string RandomString(int size)
        {
            const string chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var buffer = new char[size];

            for (var i = 0; i < size; i++)
                buffer[i] = chars[_random.Next(chars.Length)];

            return new string(buffer);
        }
    }
}
