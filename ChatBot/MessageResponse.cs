﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ChatBot
{
    /// <summary>
    /// ответ в виде числа сообщений и массива сообщений (для xml)
    /// </summary>
    [Serializable]
    [XmlRoot("response")]
    public class MessageResponse
    {
        [XmlElement("count")]
        public int count;
        [XmlArray("items")]
        [XmlArrayItem("message")]
        public Message[] items;

        public MessageResponse()
        {
            count = 0;
            items = new Message[200];
        }

        public MessageResponse(Message[] arr)
        {
            count = arr.Length;
            items = arr;
        }
    }
}
