﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using VkClasses;
using VkClasses.SerializableTypes;
using ChatAI;

namespace ChatBot
{
    /// <summary>
    /// соединение типа юзер-бот-бот-юзер
    /// </summary>
    class UserBotBotUser : Connection
    {
        public class TokenId
        {
            public string id;
            public string token;

            public TokenId(string token, string id){
                this.id = id;
                this.token = token;
            }
        }

        public TokenId[] botUser = new TokenId[2];

        public UserBotBotUser()
        {
            botUser[0] = new TokenId("", "");
            botUser[1] = new TokenId("", "");
        }

        public UserBotBotUser(MyUser user1, Bot bot1, Bot bot2, MyUser user2)
        {
            botUser[0] = new TokenId(bot1.token, user1.id.ToString());
            botUser[1] = new TokenId(bot2.token, user2.id.ToString());
        }

        public override void GetNewMessages()
        {
            for (int k = 0; k < 2; k++)
            {
                string readMessages = "";
                //берет полученные сообщения и добавляет в список, а потом делает прочитанными
                var queriesList = new NameValueCollection();
                queriesList["count"] = len.ToString();
                AppInfo.ExecuteCommand("messages.get", queriesList, botUser[k].token).Save(MessageFile);
                MessageResponse resp = LoadXml<MessageResponse>(MessageFile);
                for (int i = 0; i < len; i++)
                {
                    if (resp.items[i].read_state == 0 && resp.items[i].isOut == 0) //полученное и непрочитанное
                    {
                        readMessages += resp.items[i].id.ToString() + ", ";
                        Message mes = resp.items[i];
                        messages[k].Add(mes);
                    }
                }
            }
        }

        public override void ReadMessages()
        {
            for (int k = 0; k < 2; k++)
            {
                var queriesList = new NameValueCollection();
                if (messages[k].Count != 0)
                {
                    Thread.Sleep(1000);
                    readMessages = readMessages.Remove(readMessages.Length - 2);
                    queriesList["message_ids"] = readMessages;
                    AppInfo.ExecuteCommand("messages.markAsRead", queriesList, botUser[k].token);
                }
            }
        }

        public override void SetActivity()
        {
            base.SetActivity();
            for (int k = 0; k < 2; k++)
            {
                var setActiv = new NameValueCollection();
                if (messages[k].Count > 0)
                {
                    setActiv["user_id"] = botUser[k].id; //id
                    setActiv["type"] = "typing";
                    AppInfo.ExecuteCommand("messages.setActivity", setActiv, botUser[k].token);
                }
            }
        }

        public override void SendMessages()
        {
            base.SendMessages();
            for (int k = 0; k < 2; k++)
            {
                var queriesList = new NameValueCollection();
                for (int i = 0; i < messages[0].Count; i++)
                {
                    queriesList = new NameValueCollection();
                    queriesList["user_id"] = botUser[(k + 1) % 2].id;
                    queriesList["message"] = messages[k][i].body;
                    AppInfo.ExecuteCommand("messages.send", queriesList, botUser[(k + 1) % 2].token);
                }
                messages[k].Clear();
            }
        }

        public override void SaveDialog()
        {
            base.SaveDialog();
            string prev = "";
            string quest = "";
            int isOut = 0;
            NameValueCollection queriesList = new NameValueCollection();
            queriesList["count"] = "200";
            queriesList["user_id"] = botUser[0].id;
            AppInfo.ExecuteCommand("messages.getHistory", queriesList, botUser[0].token).Save(MessageFile);
            MessageResponse resp = LoadXml<MessageResponse>(MessageFile);
            int y = Math.Min(199, resp.count - 1);
            while (Math.Abs(resp.items[y].date - resp.items[0].date) > 200 && y > 0)
            {
                y--;
            }
            isOut = resp.items[y].isOut;
            while (y >= 0 && resp.items[y].isOut == isOut)
            {
                prev += resp.items[y].body + "\n";
                y--;
            }
            for (int i = y; i >= 0; i--)
            {
                isOut = resp.items[i].isOut;
                while (i >= 0 && resp.items[i].isOut == isOut)
                {
                    quest += resp.items[i].body + "\n";
                    i--;
                }
                if (i != 0)
                {
                    i++;
                }
                talk.AddToList(prev, quest);
                prev = quest;
                quest = "";
            }
            talk.Save(FileName);
        }
    }
}
