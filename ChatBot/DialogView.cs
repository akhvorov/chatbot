﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using VkClasses;
using VkClasses.SerializableTypes;

namespace ChatBot
{
    class DialogView : ListBox
    {
        private string ShowDialog;
        public DialogView(BotsSelection select, Bots bots)
        {
            ShowDialog = @"Xml\ShowDialogs.xml";
            this.Location = new Point(615, 12);
            this.Size = new Size(225, 200);
            //this.View = System.Windows.Forms.View.List;
            int num1 = -1, num2 = -1;
            for (int i = 0; i < bots.bots.Count; i++)
            {
                if (select.botName.Text == bots.bots[i].name)
                {
                    num1 = i;
                    for (int j = 0; j < bots.bots[i].users.Count; j++)
                    {
                        if (select.userName.Text == bots.bots[i].users[j].name)
                        {
                            num2 = j;
                            break;
                        }
                    }
                    break;
                }
            }
            if (num1 != -1 && num2 != -1)
            {
                var queriesList = new NameValueCollection();
                queriesList["count"] = "200";
                queriesList["user_id"] = bots.bots[num1].users[num2].id.ToString();
                AppInfo.ExecuteCommand("messages.getHistory", queriesList, bots.bots[num1].token).Save(ShowDialog);
                MessageResponse resp = AppForm.LoadXml<MessageResponse>(ShowDialog);
                for (int i = Math.Min(199, resp.count - 1); i >= 0; i--)
                {
                    if (resp.items[i].isOut == 0)
                    {
                        this.Items.Add(select.userName.Text + ": " + resp.items[i].body);
                    }
                    else
                    {
                        //исправить это потом на красивое что-то
                        this.Items.Add(select.botName.Text + ": " + resp.items[i].body);
                    }
                }
            }
        }
    }
}
