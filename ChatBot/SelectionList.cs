﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace ChatBot
{
    /// <summary>
    /// список рамочек, в которых происходит выбор того, как будет осуществляться работа бота
    /// </summary>
    public class SelectionList : ListView
    {
        public Bots bots;
        public List<BotsSelection> list;
        public int h;
        public string UsersAndBots;

        public SelectionList(Bots b, string UAB)
        {
            h = 90;
            bots = b;
            list = new List<BotsSelection>();
            UsersAndBots = UAB;
        }
        //передавать список ботов со всеми вытекающими
        public SelectionList(int n, Bots b, string UAB)
        {
            this.Scrollable = true;
            h = 90;
            UsersAndBots = UAB;
            this.Location = new Point(215, 14);
            this.Size = new System.Drawing.Size(855, 4*h);
            bots = b;
            list = new List<BotsSelection>();
            int k = 5;
            this.BeginUpdate();
            for (int i = 0; i < n; i++)
            {
                /*Size s = this.Size;
                if (list.Count < 4)
                {
                    s.Height = s.Height + h;
                    this.Size = s;
                }*/
                BotsSelection bot = new BotsSelection(new Point(10, k), bots, UsersAndBots);
                //this.MouseDown += new MyEventHandler(showDialog_Click);
                this.Controls.Add(bot);
                //this.Items.Add(bot);
                list.Add(bot);
                k += h;//высота каждого BotsSelection
            }
            this.EndUpdate();
        }

        void showDialog_Click(object sender, MouseEventArgs e)
        {
            for (int i = this.Controls.Count - 1; i >= 0; i--)
            {
                if (this.Controls[i] is DialogView)
                {
                    this.Controls[i].Dispose();
                }
            }
            MessageBox.Show(e.Y.ToString());
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Location.Y < e.Y && e.Y < (list[i].Location.Y + list[i].Size.Height))
                {
                    DialogView dialog = new DialogView(list[i], bots);
                    this.Controls.Add(dialog);
                }
            }
        }
        /// <summary>
        /// добавляет еще несколько рамочек
        /// </summary>
        /// <param name="n">количество добавляемых рамочек</param>
        public void Add(int n){
            int k = list.Count * h;
            this.BeginUpdate();
            for (int i = 0; i < n; i++)
            {
                /*Size s = this.Size;
                if (list.Count < 4)
                {
                    s.Height = s.Height + h;
                    this.Size = s;
                }*/
                BotsSelection bot = new BotsSelection(new Point(10, k), bots, UsersAndBots);
                bot.MouseDown += new MouseEventHandler(showDialog_Click);
                this.Controls.Add(bot);
                //this.Items.Add(bot);
                list.Add(bot);
                //if (k < h * 4)
                k += h;//высота каждого BotsSelection
            }
            this.EndUpdate();
        }
        /// <summary>
        /// убирает несколько рамочек
        /// </summary>
        /// <param name="n">кол-во рамочек</param>
        public void Remove(int n)
        {
            int l = list.Count - 1;
            this.BeginUpdate();
            while (n > 0)
            {
                /*Size s = this.Size;
                if (list.Count <= 4)
                {
                    s.Height = s.Height - h;
                    this.Size = s;
                }*/
                //list[l].Dispose();
                this.Controls.RemoveAt(l);
                list.RemoveAt(l);
                l--;
                n--;
            }
            this.EndUpdate();
        }
    }

    public class MyEventArgs : EventArgs
    {
        public string s;

        public MyEventArgs(string str)
        {
            s = str;
        }
    }
}
