﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Xml;
using System.Xml.Serialization;
using VkClasses;

namespace ChatBot
{
    /// <summary>
    /// обычный пользователь без лишних данных (используется он)
    /// </summary>
    [Serializable]
    [XmlRoot("user")]
    public class MyUser
    {
        [XmlElement("name")]
        public string name;
        /*[XmlElement("surname")]
        public string surname;*/
        [XmlElement("id")]
        public int id;

        public MyUser()
        {
            name = "";
            //surname = "";
            id = 0;
        }

        public static List<T> Get<T>(NameValueCollection queriesList)
        {
            string query = AppInfo.ComposeQuery("users.get", queriesList);
            var serializer = new XmlSerializer(typeof(UsersGetResponse<T>));
            //MessageBox.Show(AppInfo.ExecuteCommand("users.get", queriesList).InnerXml);
            var response = (UsersGetResponse<T>)serializer.Deserialize(new XmlTextReader(query));
            return response.items;
        }
    }
}
