﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ChatBot
{
    /// <summary>
    /// список ботов (чтобы удобнее xml читать было)
    /// </summary>
    [Serializable]
    [XmlRoot("bots")]
    public class Bots
    {
        [XmlArray("bots")]
        [XmlArrayItem("bot")]
        public List<Bot> bots;

        public Bots()
        {
            bots = new List<Bot>();
        }

        public void GetAllFriends(){
            List<string> toAdd = new List<string>();
            for (int i = 0; i < bots.Count; i++)
            {
                var queriesList = new NameValueCollection();
                queriesList["user_id"] = bots[i].id.ToString();
                toAdd = FriendsResponse.Get(queriesList);
                for (int j = 0; j < bots[j].users.Count; j++)
                    for (int k = toAdd.Count - 1; k >= 0; k++)
                        if (bots[i].users[j].id.ToString() == toAdd[k])
                        {
                            toAdd.RemoveAt(k);
                            break;
                        }
                foreach(string s in toAdd)
                    bots[i].AddFriend(s, false);
            }
        }
    }
}
