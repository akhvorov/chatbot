﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ChatAI;
using VkClasses;
using VkClasses.SerializableTypes;


namespace ChatBot
{
    public partial class AppForm : Form
    {
        public string UsersAndBots;
        //string Bots;
        //string NewUserFile;
        //string UsersFile;
        //List<Bot> bots;
        public Bots bots;
        public SelectionList selectionList;
        public List<Connection> connection;

        public AppForm()
        {
            InitializeComponent();
            UsersAndBots = @"..\..\..\ChatBot\Xml\Bots.xml";
            connection = new List<Connection>(); //список соединений бот-человек или челове-бот-бот-человек
            bots = LoadXml<Bots>(UsersAndBots); //список ботов и их друзей
            //bots.GetAllFriends();
            selectionList = new SelectionList(1, bots, UsersAndBots); //отвечает за рамочки для выбора ботов
            this.Controls.Add(selectionList);
            //usersAndBots = LoadXml<Couples>(UsersAndBots).MakeDictionary();
            timer1.Interval = 5000;
            timer2.Interval = 60000; //1 минута
            //timer3.Interval = 500;
            //timer3.Start();
            //send.Click += new System.EventHandler(send_Click);
            timer1.Tick += timer1_Tick;
        }

        //TODO: Сделать класс отвечающий за каждую систему общения
        //Добавить все методы туда, но разбить на несколько, чтоб запросы уходили на разные и не было ошибок
        //Тут сделать список элементов каждого из этих классов и гонять их по циклу
        //Придумать, как их объявлять и назначать каждому своего
        //Сделать Dictionary, чтобы называть имена, а выводить id или token

        public static T LoadXml<T>(string fileName)
        {
            T temp;
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new StreamReader(fileName))
                temp = (T)serializer.Deserialize(stream);
            return temp;
        }
        public static void SaveXml<T>(string fileName, T variable)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new StreamWriter(fileName))
                serializer.Serialize(stream, variable);
        }
        /// <summary>
        /// что происходит каждый раз
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < connection.Count; i++)
            {
                connection[i].GetNewMessages();
                Thread.Sleep(350);
                connection[i].ReadMessages();
                Thread.Sleep(350);
                connection[i].SetActivity();
                Thread.Sleep(350);
                connection[i].SendMessages();
                Thread.Sleep(350);
            }
            /*for (int i = 0; i < connection.Count; i++)
            {
                Thread.Sleep(1000);
                connection[i].GetNewMessages();
            }
            for (int i = 0; i < connection.Count; i++)
            {
                //connection[i].ReadMessages();
            }
            for (int i = 0; i < connection.Count; i++)
            {
                //connection[i].SetActivity();
            }
            for (int i = 0; i < connection.Count; i++)
            {
                Thread.Sleep(1000);
                connection[i].SendMessages();
            }*/
        }
        /// <summary>
        /// нажатие на кнопку Старт/Стоп (включатель всего)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerButton_Click(object sender, EventArgs e)
        {
            if (timerButton.Text == "Старт")
            {
                int num1 = -1, num2 = -1, num3 = -1, num4 = -1;
                for (int i = 0; i < selectionList.list.Count; i++)
                {
                    if (selectionList.list[i].botName.Text != "" && selectionList.list[i].userName.Text != "")
                        for (int j = 0; j < bots.bots.Count; j++)
                            if (bots.bots[j].name == selectionList.list[i].botName.Text)
                            {
                                num1 = j;
                                for (int k = 0; k < bots.bots[j].users.Count; k++)
                                    if (bots.bots[j].users[k].name == selectionList.list[i].userName.Text)
                                        num2 = k;
                            }
                    else
                    {
                        MessageBox.Show("не заполнено какое-то поле нормально");
                        continue;
                    }
                    if (selectionList.list[i].connectionSelection.Text == "1 x 1")
                    {
                        if (num1 == -1 || num2 == -1)
                            MessageBox.Show("что-то не так с индексами в таймере");
                        else
                            connection.Add(new BotUser(bots.bots[num1], bots.bots[num1].users[num2]));
                    }
                    else { 
                        if (selectionList.list[i].botName2.Text != "" && selectionList.list[i].userName2.Text != "")
                        {
                            for (int j = 0; j < bots.bots.Count; j++)
                                if (bots.bots[j].name == selectionList.list[i].botName2.Text)
                                {
                                    num3 = j;
                                    for (int k = 0; k < bots.bots[j].users.Count; k++)
                                        if (bots.bots[j].users[k].name == selectionList.list[i].userName2.Text)
                                            num4 = k;
                                }
                            if (num3 == -1 || num4 == -1)
                                MessageBox.Show("что-то не так с индексами в таймере");
                            else
                                connection.Add(new UserBotBotUser(bots.bots[num1].users[num2], bots.bots[num1], bots.bots[num3], bots.bots[num3].users[num4]));
                        }
                    }
                }
                timer1.Start();
                timerButton.Text = "Стоп";
            }
            else
            {
                for (int i = 0; i < connection.Count; i++)
                {
                    //connection[i].SaveDialog();
                }
                connection.Clear();
                timer1.Stop();
                timerButton.Text = "Старт";
            }
        }
        /// <summary>
        /// таймер на то, чтобы сохранить новые реплики в диалоге
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < connection.Count; i++)
            {
                //connection[i].SaveDialog();
            }
            SaveXml<Bots>(UsersAndBots, bots);
        }
        
        /// <summary>
        /// сохраняет новые синапсы ко всем
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveDialogButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < connection.Count; i++)
            {
                //connection[i].SaveDialog();
            }
        }
        /// <summary>
        /// добавление/удаление связей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int l = selectionList.list.Count;
            int n = (int)numericUpDown1.Value;
            if (l > n)
                for (int i = l - 1; i >= n; i--)
                    selectionList.Remove(i);
            else if (l < n)
                selectionList.Add(n - l);
            else
            {}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var querieList = new NameValueCollection();
            querieList["user_ids"] = "264219013,46442269,1,2,3,4,5";
            querieList["fields"] = "first_name,last_name,sex";
            var users = MyUser.Get<UserInfo>(querieList);
            MessageBox.Show(users.Aggregate("", (current, singleUser) => current + singleUser.ToString()));
        }
    }
}
