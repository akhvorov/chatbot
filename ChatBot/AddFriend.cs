﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace ChatBot
{
    /// <summary>
    /// рамочка, которая добавляет в "друзья" к выбранному боту
    /// </summary>
    public class AddFriend : GroupBox
    {
        public ComboBox bot;
        public TextBox user_id;
        public Button ok;
        public Bots bots;
        public Button cancel;

        public AddFriend(Point p, Bots botss, string botName)
        {
            /*Point point = new Point(840, p.Y);
            this.Location = point;*/
            this.Location = p;
            this.Size = new Size(250, 70);

            bots = botss;

            bot = new ComboBox();
            bot.Location = new Point(10, 10);
            bot.Size = new Size(150, 20);
            bot.Text = botName;
            for (int i = 0; i < bots.bots.Count; i++)
            {
                bot.Items.Add(bots.bots[i].name);
            }
            this.Controls.Add(bot);

            user_id = new TextBox();
            user_id.Location = new Point(10, 35);
            user_id.Size = new Size(150, 20);
            this.Controls.Add(user_id);

            ok = new Button();
            ok.Location = new Point(170, 35);
            ok.Size = new Size(70, 25);
            ok.Text = "Ок";
            this.Controls.Add(ok);
            ok.Click += ok_Click;

            cancel = new Button();
            cancel.Text = "Отмена";
            cancel.Location = new Point(170, 5);
            cancel.Size = new Size(70, 25);
            this.Controls.Add(cancel);
            cancel.Click += cancel_Click;
        }
        /// <summary>
        /// отмена действия
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        /// <summary>
        /// добавляет нового друга боту
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ok_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < bots.bots.Count; i++)
            {
                if (bots.bots[i].name == bot.Text)
                {
                    bots.bots[i].AddFriend(user_id.Text, true);
                }
            }
            this.Dispose();
        }
    }
}
