﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using ChatAI;

namespace ChatBot
{
    /// <summary>
    /// общий вид обоих типов соединения
    /// </summary>
    public abstract class Connection
    {
        public List<Message>[] messages; //сообщения, на которые надо ответить
        public string question;
        public string previous;
        public string FileName;
        public string MessageFile;
        public int len;
        public string readMessages;
        public Talk talk;

        public Connection()
        {
            FileName = @"Xml\Диалоги.xml";
            MessageFile = @"Xml\Messages.xml";
            previous = "";
            question = "";
            len = 15;
            readMessages = "";
            talk = new Talk();
            if (File.Exists(FileName))
                talk = new Talk(FileName);
            Talk.MakeSlovar();
            messages = new List<Message>[2];
            messages[0] = new List<Message>();
            messages[1] = new List<Message>();
        }

        /// <summary>
        /// Сериализатор XML в тип T по файлу
        /// </summary>
        public T LoadXml<T>(string fileName)
        {
            T temp;
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new StreamReader(fileName))
                temp = (T)serializer.Deserialize(stream);
            return temp;
        }

        /// <summary>
        /// сохраняет в XML файл
        /// </summary>
        /// <param name="fileName">куда сохранить</param>
        /// <param name="variable">что сохранить</param>
        public void SaveXml<T>(string fileName, T variable)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new StreamWriter(fileName))
                serializer.Serialize(stream, variable);
        }

        /// <summary>
        /// получает новые сообщения и добавляет в списки
        /// </summary>
        public virtual void GetNewMessages()
        {

        }

        /// <summary>
        /// делает сообщения прочитанными (не очень нужно, это происходит автоматически при ответе)
        /// </summary>
        public virtual void ReadMessages()
        {

        }

        /// <summary>
        /// делает пометочку "кто-то набирает сообщение"
        /// </summary>
        public virtual void SetActivity()
        {

        }

        /// <summary>
        /// отправляет сообщения
        /// </summary>
        public virtual void SendMessages()
        {

        }

        /// <summary>
        /// сохраняет обновления в базу синапсов
        /// </summary>
        public virtual void SaveDialog()
        {

        }

        /// <summary>
        /// последнее отправленное сообщение пользователю с таким id
        /// </summary>
        /// <param name="userID">id того пользователя</param>
        /// <returns>сообщение</returns>
        protected Message LastMessageTo(int userID)
        {
            for (int k = 0; k < messages[1].Count; k++)
            {
                if (messages[1][k].user_id == userID)
                {
                    return messages[1][k];
                }
            }
            return messages[1][0];
        }

        /// <summary>
        /// последнее полученное сообщение от юзера
        /// </summary>
        /// <param name="listToOneID">списко полученных сообщений</param>
        /// <returns>последнее сообщение</returns>
        protected Message LastMessageFrom(List<Message> listToOneID)
        {
            Message inMes = listToOneID[0];
            for (int k = 0; k < listToOneID.Count; k++)
                if (inMes.date < listToOneID[k].date)
                    inMes = listToOneID[k];
            return inMes;
        }

    }
}
