﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ChatBot
{
    /// <summary>
    /// ответ в виде списка пользователей (для xml-файла)
    /// </summary>
    [Serializable]
    [XmlRoot("response")]
    public class UserResponse
    {
        [XmlArray("items")]
        [XmlArrayItem("user")]
        public List<User> items;

        public UserResponse()
        {
            items = new List<User>();
        }

        public UserResponse(List<User> arr)
        {
            items = arr;
        }
    }
}
