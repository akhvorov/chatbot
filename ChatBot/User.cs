﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using VkClasses;
using VkClasses.SerializableTypes;

namespace ChatBot
{
    /// <summary>
    /// пользователь для получения xml-ответа
    /// </summary>
    [Serializable]
    [XmlRoot("user")]
    public class User
    {
        [XmlElement("id")]
        public int id;
        [XmlElement("first_name")]
        public string name;
        [XmlElement("last_name")]
        public string surname;
        [XmlElement("sex")]
        public int sex; //0 - не указано, 1 - женский, 2 - мужской
        [XmlElement("bdate")]
        public string bdate;
        [XmlElement("city")]
        public City city;
        [XmlElement("online")]
        public int online; //1 - в сети, 0 - не в сети

        string lastMessage;

        public User()
        {
            id = 0;
            name = "";
            surname = "";
            sex = 0;
            bdate = "";
            city = new City();
            online = 0;
            lastMessage = "";
        }

        public User(int user_id, string NewUserFile)
        {
            id = user_id;
            var queriesList = new NameValueCollection();
            queriesList["user_ids"] = user_id.ToString();
            queriesList["fields"] = "first_namr, last_name, sex, bdate, city, online";
            AppInfo.ExecuteCommand("messages.get", queriesList, "").Save(NewUserFile);
        }

        //не работает
        /// <summary>
        /// должен отправлять запрос на получение данных пользователя и выдавать готовый ответ
        /// </summary>
        /// <param name="queriesList">параметры</param>
        /// <returns>ответ</returns>
        public static UserResponse Get(NameValueCollection queriesList)
        {
            var query = AppInfo.ComposeQuery("users.get", queriesList);
            var serializer = new XmlSerializer(typeof(UserResponse));
            UserResponse response = (UserResponse)serializer.Deserialize(new XmlTextReader(query));
            return response;
        }
    }


    [Serializable]
    [XmlRoot("city")]
    public class City
    {
        [XmlElement("id")]
        int id;
        [XmlElement("title")]
        string title;

        public City()
        {
            id = 0; 
            title = "";
        }
    }
}
