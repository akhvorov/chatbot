﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using VkClasses;
using VkClasses.SerializableTypes;

namespace ChatBot
{
    /// <summary>
    /// рамочка, в которой происходит выбор параметров для Connection
    /// </summary>
    public class BotsSelection : GroupBox
    {
        //TODO: Разбивать на отдельные методы, много повторений
        public Bots bots;
        public Connection connection;
        public Label connectionSelectionLabel;
        public ComboBox connectionSelection;
        public Label botNameLabel;
        public ComboBox botName;
        public Label userNameLabel;
        public ComboBox userName;
        public Button addFriend;
        public Label botNameLabel2;
        public ComboBox botName2;
        public Label userNameLabel2;
        public ComboBox userName2;
        public Button addFriend2;
        public Button showDialog;
        public string UsersAndBots;
        //public string ShowDialog;

        public BotsSelection(Point point, Bots b, string UAB)
        {
            bots = b;
            UsersAndBots = UAB;
            //ShowDialog = @"Xml\ShowDialogs.xml";
            connectionSelectionLabel = new Label();
            connectionSelection = new ComboBox();
            botNameLabel = new Label();
            botName = new ComboBox();
            addFriend = new Button();
            botNameLabel2 = new Label();
            botName2 = new ComboBox();
            userName = new ComboBox();
            userNameLabel = new Label();
            userName2 = new ComboBox();
            userNameLabel2 = new Label();
            addFriend2 = new Button();
            showDialog = new Button();
            this.Controls.Add(connectionSelectionLabel);
            this.Controls.Add(connectionSelection);
            this.Controls.Add(botNameLabel);
            this.Controls.Add(botName);
            this.Controls.Add(addFriend);
            this.Controls.Add(botNameLabel2);
            this.Controls.Add(botName2);
            this.Controls.Add(userName);
            this.Controls.Add(userName2);
            this.Controls.Add(userNameLabel);
            this.Controls.Add(userNameLabel2);
            this.Controls.Add(addFriend2);
            this.Controls.Add(showDialog);

            //GroupBox
            this.Size = new System.Drawing.Size(600, 85);
            this.Location = point;

            //connectionSelectionLabel
            connectionSelectionLabel.Text = "Тип подключения";
            connectionSelectionLabel.Location = new Point(5, 5);
            connectionSelectionLabel.Size = new Size(60, 15);

            //connectionSelection
            connectionSelection.Items.Add("1 x 1");
            connectionSelection.Items.Add("2 x 2");
            connectionSelection.Text = "1 x 1";
            connectionSelection.Location = new Point(5, 30);
            connectionSelection.Size = new Size(60, 15);
            connectionSelection.SelectedValueChanged += connectionSelection_SelectedValueChanged;

            //botNameLabel
            botNameLabel.Text = "Имя бота:";
            botNameLabel.Location = new Point(75, 5);
            botNameLabel.Size = new Size(90, 15);

            //userNameLabel
            userNameLabel.Text = "Имя юзера:";
            userNameLabel.Location = new Point(75, 30);
            userNameLabel.Size = new Size(90, 15);

            //botName - имя первого бота
            botName.Text = "Денис Галаган";
            botName.Location = new Point(165, 5);
            botName.Size = new Size(120, 15);
            for (int i = 0; i < bots.bots.Count; i++)
                botName.Items.Add(bots.bots[i].name);
            botName.SelectedValueChanged += botName_SelectedValueChanged;

            //userName - имя первого юзера
            userName.Text = "Саша Хворов";
            userName.Location = new Point(165, 30);
            userName.Size = new Size(120, 15);

            //addFriend - добавление друга к первому боту
            addFriend.Text = "Добавить друга";
            addFriend.Location = new Point(165, 55);
            addFriend.Size = new System.Drawing.Size(120, 25);
            addFriend.Click += addFriend_Click;

            //botNameLabel2
            botNameLabel2.Text = "Имя бота:";
            botNameLabel2.Location = new Point(295, 5);
            botNameLabel2.Size = new Size(90, 15);
            botNameLabel2.Visible = false;

            //userNameLabel2
            userNameLabel2.Text = "Имя юзера:";
            userNameLabel2.Location = new Point(295, 30);
            userNameLabel2.Size = new Size(90, 15);
            userNameLabel2.Visible = false;

            //botName2 - имя второго бота, если такой есть
            botName2.Location = new Point(385, 5);
            botName2.Size = new Size(120, 15);
            botName2.Visible = false;
            int num2 = -1;
            for (int i = 0; i < bots.bots.Count; i++) //находит номер первого бота
                if (bots.bots[i].name == botName.Text)
                    num2 = i;
            if (num2 != -1)//если он уже выбран, то дает список без уже выбранного
                for (int i = 0; i < bots.bots.Count; i++)
                    if (i != num2)
                        botName2.Items.Add(bots.bots[i].name);
            botName2.SelectedValueChanged += botName2_SelectedValueChanged;
            

            //userName2
            userName2.Text = "";
            userName2.Location = new Point(385, 30);
            userName2.Size = new Size(120, 15);
            userName2.Visible = false;

            //addFriend2 - добавление друга ко второму боту
            addFriend2.Text = "Добавить друга";
            addFriend2.Location = new Point(385, 55);
            addFriend2.Size = new System.Drawing.Size(120, 25);
            addFriend2.Visible = false;
            addFriend2.Click += addFriend2_Click;

            //showDialog - показывает последние сообщения в диалоге
            showDialog.Text = "Диалог>>";
            showDialog.Location = new Point(510, 30);
            showDialog.Size = new Size(85, 25);
            showDialog.Click += showDialog_Click;
        }
        /// <summary>
        /// если выбрать другого второго бота
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void botName2_SelectedValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            userName2.Items.Clear();
            int num3 = -1;
            for (int i = 0; i < bots.bots.Count; i++)//находит номер второго бота
                if (bots.bots[i].name == botName2.Text)
                    num3 = i;
            bool b = true;
            for (int i = 0; i < bots.bots[num3].users.Count; i++)
                if (bots.bots[num3].users[i].name == userName2.Text)
                    b = false;
            if (b)
                userName2.Text = "";
            if (num3 != -1)
                for (int i = 0; i < bots.bots[num3].users.Count; i++)
                    userName2.Items.Add(bots.bots[num3].users[i].name);
        }
        /// <summary>
        /// если выбрать другой тип соединения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void connectionSelection_SelectedValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            if (connectionSelection.Text == "1 x 1")
            {
                botName2.Visible = false;
                botNameLabel2.Visible = false;
                userName2.Visible = false;
                userNameLabel2.Visible = false;
                addFriend2.Visible = false;
            }
            if (connectionSelection.Text == "2 x 2")
            {
                botName2.Visible = true;
                botNameLabel2.Visible = true;
                userName2.Visible = true;
                userNameLabel2.Visible = true;
                addFriend2.Visible = true;
            }
        }
        //TODO: Дописать!
        //сделать отдельный класс и вынести в AppForm, так как иначе не будет видно
        //функция, которая будет красиво делать строку исходящих сообщений
        //возможно это должен быть ListBox или ListView
        void showDialog_Click(object sender, EventArgs e)
        {
            //AppForm.ShowDialog();


            //throw new NotImplementedException();
           /* RichTextBox rtb = new RichTextBox();
            int num1 = -1, num2 = -1;
            for (int i = 0; i < bots.bots.Count; i++)
            {
                if (botName.Text == bots.bots[i].name)
                {
                    num1 = i;
                    for (int j = 0; j < bots.bots[i].users.Count; j++)
                    {
                        if (userName.Text == bots.bots[i].users[j].name)
                        {
                            num2 = j;
                            break;
                        }
                    }
                    break;
                }
            }
            if (num1 != -1 && num2 != -1)
            {
                var queriesList = new NameValueCollection();
                queriesList["count"] = "200";
                queriesList["user_id"] = bots.bots[num1].users[num2].id.ToString();
                AppInfo.ExecuteCommand("messeges.getHistory", queriesList, bots.bots[num1].token).Save(ShowDialog);
                MessageResponse resp = AppForm.LoadXml<MessageResponse>(ShowDialog);
                for (int i = resp.count - 1; i >= 0; i--)
                {
                    if(resp.items[i].id == bots.bots[num1].users[num2].id){
                        rtb.Text += resp.items[i].body;
                    }
                    else{
                        //исправить это потом на красивое что-то
                        rtb.Text += "   :" + resp.items[i].body;
                    }
                }
            }*/
        }
        /// <summary>
        /// вызывает рамочку добавления друга боту
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void addFriend_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            AddFriend add = new AddFriend(botNameLabel.Location, bots, botName.Text);
            this.Controls.Add(add);
            add.BringToFront();
            AppForm.SaveXml<Bots>(UsersAndBots, bots);
        }
        /// <summary>
        /// вызывает рамочку добавления друга боту2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void addFriend2_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            AddFriend add = new AddFriend(botNameLabel2.Location, bots, botName2.Text);
            this.Controls.Add(add);
            add.BringToFront();
            AppForm.SaveXml<Bots>(UsersAndBots, bots);
        }
        /// <summary>
        /// когда выбираешь нового первого бота
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void botName_SelectedValueChanged(object sender, EventArgs e)
        {
            botName2.Items.Clear();
            userName.Items.Clear();
            userName2.Items.Clear();
            if (botName.Text == botName2.Text)
            {
                botName2.Text = "";
                userName2.Text = "";
            }
            //юзер
            int num1 = -1;
            for (int i = 0; i < bots.bots.Count; i++) //находит выбранного
                if (bots.bots[i].name == botName.Text)
                    num1 = i;
            bool b = true;
            for (int i = 0; i < bots.bots[num1].users.Count; i++)
                if (bots.bots[num1].users[i].name == userName.Text)
                    b = false;
            if (b)
                userName.Text = "";
            if (num1 != -1) //если выбран, то дает список
                for (int i = 0; i < bots.bots[num1].users.Count; i++)
                    userName.Items.Add(bots.bots[num1].users[i].name);
            //второй бот
            int num2 = -1;
            for (int i = 0; i < bots.bots.Count; i++) //находит номер первого бота
                if (bots.bots[i].name == botName.Text)
                    num2 = i;
            if (num2 != -1)//если он уже выбран, то дает список без уже выбранного
                for (int i = 0; i < bots.bots.Count; i++)
                    if (i != num2)
                        botName2.Items.Add(bots.bots[i].name);
            //второй юзер
            int num3 = -1;
            for (int i = 0; i < bots.bots.Count; i++)//находит номер второго бота
                if (bots.bots[i].name == botName2.Text)
                    num3 = i;
            if (num3 != -1)
                for (int i = 0; i < bots.bots[num3].users.Count; i++)
                    userName2.Items.Add(bots.bots[num3].users[i].name);
        }
        /// <summary>
        /// обновляет данные, которые должны содержаться в списках выбирания
        /// </summary>
        public void UpdateLists()
        {
            botName.Items.Clear();
            botName2.Items.Clear();
            userName.Items.Clear();
            userName2.Items.Clear();
            //бот
            for (int i = 0; i < bots.bots.Count; i++)
                botName.Items.Add(bots.bots[i].name);
            //юзер
            int num1 = -1;
            for (int i = 0; i < bots.bots.Count; i++) //находит выбранного
                if (bots.bots[i].name == botName.Text)
                    num1 = i;
            if (num1 != -1) //если выбран, то дает список
                for (int i = 0; i < bots.bots[num1].users.Count; i++)
                    userName.Items.Add(bots.bots[num1].users[i].name);
            //второй бот
            int num2 = -1;
            for (int i = 0; i < bots.bots.Count; i++) //находит номер первого бота
                if (bots.bots[i].name == botName.Text)
                    num2 = i;
            if (num2 != -1)//если он уже выбран, то дает список без уже выбранного
                for (int i = 0; i < bots.bots.Count; i++)
                    if (i != num2)
                        botName2.Items.Add(bots.bots[i].name);
            //второй юзер
            int num3 = -1;
            for (int i = 0; i < bots.bots.Count; i++)//находит номер второго бота
                if (bots.bots[i].name == botName2.Text)
                    num3 = i;
            if (num3 != -1)
                for (int i = 0; i < bots.bots[num3].users.Count; i++)
                    userName2.Items.Add(bots.bots[num3].users[i].name);
        }
    }
}
