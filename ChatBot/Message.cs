﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ChatBot
{
    /// <summary>
    /// сообщение
    /// </summary>
    public class Message
    {
        [XmlElement("id")]
        public int id;
        [XmlElement("date")]
        public int date;
        [XmlElement("out")]
        public int isOut;
        [XmlElement("user_id")]
        public int user_id;
        [XmlElement("read_state")]
        public int read_state;
        /*[XmlElement("from_id")]
        public int from_id;*/
        [XmlElement("title")]
        public string title;
        [XmlElement("body")]
        public string body;
        /*[XmlArray("attachments")]
        [XmlArrayItem("attachment")]
        public List<Attachments> attachments;
        [XmlArray("fwd_messages")]
        [XmlArrayItem("message")]
        public List<Fwd_Messages> fwd_messages;*/

        public string previos;

        public Message()
        {
            id = 0;
            user_id = 0;
            //from_id = 0;
            date = 0;
            read_state = 0;
            isOut = 0;
            title = "";
            body = "";
            previos = "";
            /*attachments = new List<Attachments>();
            fwd_messages = new List<Fwd_Messages>();*/
        }
    }

    public class Fwd_Messages
    {
        [XmlElement("attachments")]
        public Attachments attachments;

        public Fwd_Messages()
        {
            attachments = new Attachments();
        }
    }

    public class Attachments
    {
        [XmlElement("type")]
        string type;
        [XmlElement("photo")]
        Photo photo;

        public Attachments()
        {
            type = "";
            photo = new Photo();
        }
    }

    public class Photo
    {
        [XmlElement("photo_75")]
        string photo_75;
        [XmlElement("photo_807")]
        string photo_807;

        public Photo()
        {
            photo_75 = "";
            photo_807 = "";
        }
    }
}
