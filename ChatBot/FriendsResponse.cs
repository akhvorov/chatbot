﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using VkClasses;

namespace ChatBot
{
    [Serializable]
    [XmlRoot ("response")]
    public class FriendsResponse
    {
        [XmlElement ("count")]
        public int count;
        [XmlElement ("items")]
        public List<string> items;

        public FriendsResponse()
        {
            count = 0;
            items = new List<string>();
        }

        public static List<string> Get(NameValueCollection queriesList)
        {
            string query = AppInfo.ComposeQuery("friends.get", queriesList);
            var serializer = new XmlSerializer(typeof(FriendsResponse));
            var response = (FriendsResponse)serializer.Deserialize(new XmlTextReader(query));
            return response.items;
        }
    }
}
