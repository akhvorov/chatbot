﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Serialization;
using VkClasses;
using VkClasses.SerializableTypes;
using ChatAI;

namespace ChatBot
{
    /// <summary>
    /// соединение типа бот-человек
    /// </summary>
    public class BotUser : Connection
    {
        public string id;
        public string token;

        public BotUser()
        {
            id = "";
            token = "";
        }

        public BotUser(Bot bot, MyUser user)
        {
            id = user.id.ToString();
            token = bot.token;
            FileName = @"..\..\..\ChatBot\Xml\Диалоги.xml";
            MessageFile = @"..\..\..\ChatBot\Xml\Messages.xml";
            previous = "";
            question = "";
            len = 15;
            messages = new List<Message>[2];
            messages[0] = new List<Message>();
            messages[1] = new List<Message>();
        }
        /// <summary>
        /// считывает новые сообщения (полученные в message[0], отправленные в message[1])
        /// </summary>
        public override void GetNewMessages()
        {
            base.GetNewMessages();
            /*var queriesList = new NameValueCollection();
            MessageResponse resp = new MessageResponse();
            queriesList["count"] = len.ToString();
            try
            {
                AppInfo.ExecuteCommand("messages.get", queriesList, token).Save(MessageFile);
                resp = LoadXml<MessageResponse>(MessageFile);
            }
            catch (System.InvalidOperationException e)
            {
                Thread.Sleep(1000);
                AppInfo.ExecuteCommand("messages.get", queriesList, token).Save(MessageFile);
                resp = LoadXml<MessageResponse>(MessageFile);
            }*/
            var queriesList = new NameValueCollection();
            queriesList["count"] = len.ToString();
            AppInfo.ExecuteCommand("messages.get", queriesList, token).Save(MessageFile);
            MessageResponse resp = LoadXml<MessageResponse>(MessageFile);
            queriesList = new NameValueCollection();
            for (int i = 0; i < len; i++)
            {
                if (resp.items[i].read_state == 0 && resp.items[i].isOut == 0) //полученное и непрочитанное
                {
                    MessageBox.Show("New message: " + resp.items[i].body);
                    readMessages += resp.items[i].id.ToString() + ", ";
                    Message mes = resp.items[i];
                    messages[0].Add(mes);
                }
            }
            //MessageBox.Show(messages[0].Count.ToString());
            queriesList = new NameValueCollection();
            queriesList["out"] = "1";
            queriesList["count"] = len.ToString();
            AppInfo.ExecuteCommand("messages.get", queriesList, token).Save(MessageFile);
            resp = LoadXml<MessageResponse>(MessageFile);
            for (int i = 0; i < len; i++)
                messages[1].Add(resp.items[i]);
        }
        /// <summary>
        /// делает сообщения прочитанными
        /// </summary>
        public override void ReadMessages()
        {
            base.ReadMessages();
            var queriesList = new NameValueCollection();
            if (messages[0].Count != 0)
            {
                Thread.Sleep(1000);
                readMessages = readMessages.Remove(readMessages.Length - 2);
                queriesList["message_ids"] = readMessages;
                AppInfo.ExecuteCommand("messages.markAsRead", queriesList, token);
            }
        }
        /// <summary>
        /// делает надпись "бот набирает сообщение.."
        /// </summary>
        public override void SetActivity()
        {
            base.SetActivity();
            if (messages[0].Count > 0)
            {
                var setActiv = new NameValueCollection();
                setActiv["user_id"] = id; //id
                setActiv["type"] = "typing";
                AppInfo.ExecuteCommand("messages.setActivity", setActiv, token);
            }
        }
        /// <summary>
        /// отправляет сообщения
        /// </summary>
        public override void SendMessages()
        {
            base.SendMessages();
            List<Message> listToOneID = new List<Message>();
            int userID;
            Message outMes = new Message();
            for (int i = messages[0].Count - 1; i >= 0 && i < messages[0].Count; i--)
            {
                userID = messages[0][i].user_id;
                while (i >= 0 && messages[0][i].user_id == userID) //составляем список сообщений от одного пользователя
                {
                    listToOneID.Add(messages[0][i]);
                    i--;
                }
                if (i != 0)
                    i++;
                outMes = LastMessageTo(userID);
                Message inMes = LastMessageFrom(listToOneID);
                if (outMes.date > inMes.date) //если последнее сообщение от бота, то отвечать не надо
                {
                    //MessageBox.Show("Пропускает");
                    listToOneID.Clear();
                    outMes = new Message();
                    continue;
                }
                var queriesList = new NameValueCollection();
                queriesList["user_id"] = userID.ToString();
                if (Talk.TheEndOfInput(question))
                {
                    queriesList["message"] = "Пока!";
                    AppInfo.ExecuteCommand("messages.send", queriesList, token);
                    continue;
                }
                for (int k = 0; k < listToOneID.Count; k++) //объединил все вопросы
                    question += listToOneID[k].body + " \n";
                previous = outMes.body;
                string messageText = "";

                //!!! это важно, здесь он обучается!!!
                //messageText = talk.GetAnswer(previous, question); //научил отвечать на предыдущюю реплику объединением
                //!!!

                messageText = "";
                for (int k = 0; k < listToOneID.Count; k++) //пишем, как отвечать, но не обучая, потому что объединяем ответы на каждую из подсообщений
                {
                    messageText += talk.GetAnswer(previous, listToOneID[k].body, false) + "\n";
                    //MessageBox.Show(listToOneID.Count.ToString() +" + " + listToOneID[k].body);
                }
                queriesList["message"] = messageText;
                MessageBox.Show("Итог: " + messageText);
                AppInfo.ExecuteCommand("messages.send", queriesList, token);
                talk.Save(FileName);
                listToOneID.Clear();
                previous = "";
                question = "";
                outMes = new Message();
                //messages[0].Clear();
            }
            messages[0].Clear();
        }
        //он вроде по ходу дела сам учится
        /// <summary>
        /// сохраняет обновления в диалоги
        /// </summary>
        /*public override void SaveDialog()
        {
            base.SaveDialog();
            string prev = "";
            string quest = "";
            int isOut = 0;
            NameValueCollection queriesList = new NameValueCollection();
            queriesList["count"] = "200";
            queriesList["user_id"] = id;
            AppInfo.ExecuteCommand("messages.getHistory", queriesList, token).Save(MessageFile);
            MessageResponse resp = LoadXml<MessageResponse>(MessageFile);
            int y = Math.Min(199, resp.count - 1);
            while (Math.Abs(resp.items[y].date - resp.items[0].date) > 200 && y > 0)
            {
                y--;
            }
            isOut = resp.items[y].isOut;
            while (y >= 0 && resp.items[y].isOut == isOut)
            {
                prev += resp.items[y].body + "\n";
                y--;
            }
            for (int i = y; i >= 0; i--)
            {
                isOut = resp.items[i].isOut;
                while (i >= 0 && resp.items[i].isOut == isOut)
                {
                    quest += resp.items[i].body + "\n";
                    i--;
                }
                if (i != 0)
                {
                    i++;
                }
                talk.AddToList(prev, quest);
                prev = quest;
                quest = "";
            }
            talk.Save(FileName);
        }*/
    }
}
