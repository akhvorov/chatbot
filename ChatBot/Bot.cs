﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using VkClasses;
using VkClasses.SerializableTypes;
using ChatAI;

namespace ChatBot
{
    /// <summary>
    /// бот, содержащий всю информацию о себе
    /// </summary>
    [Serializable]
    [XmlRoot("bot")]
    public class Bot
    {
        [XmlElement("name")]
        public string name;
        [XmlElement("id")]
        public int id;
        [XmlElement("token")]
        public string token;
        [XmlElement("real_id")]
        public int real_id;
        [XmlElement("login")]
        public string login;
        [XmlElement("password")]
        public string password;
        [XmlArray("users")]
        [XmlArrayItem("user")]
        public List<MyUser> users;


        public Bot()
        {
            name = "";
            id = 0;
            token = "";
            real_id = 0;
            login = "";
            password = "";
            users = new List<MyUser>();
        }
        /// <summary>
        /// добавляет пользователя в свой список
        /// </summary>
        /// <param name="user"></param>
        public void AddFriend(MyUser user){
            bool b = true;
            for(int i = 0; i < users.Count; i++){
                if(users[i] == user){
                    b = false;
                }
            }
            if(b){
                users.Add(user);
            }
        }
        /// <summary>
        /// по id должен получать имя пользователя и добавлять в список юзеров бота
        /// </summary>
        /// <param name="id">id</param>
        public void AddFriend(string id, bool needToVK)
        {
            var queriesList = new NameValueCollection();
            queriesList["ids"] = id.ToString();
            //AppInfo.ExecuteCommand("users.get", queriesList, token).Save(MessageFile);
            List<UserInfo> list = MyUser.Get<UserInfo>(queriesList);
            MyUser myUser = new MyUser();
            myUser.id = list[0].id;
            myUser.name = list[0].FirstName;
            //myUser.surname= response.items[0].surname;
            users.Add(myUser);
            if (needToVK)
            {
                queriesList = new NameValueCollection();
                queriesList["id"] = id;
                AppInfo.ExecuteCommand("friends.add", queriesList, this.token);
            }
        }
    }
}
