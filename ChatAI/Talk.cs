﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows.Forms;
using System.IO;


namespace ChatAI
{
    public class Talk
    {
        public List<Sinaps> Sinapses;
        public static List<List<string>> Slovar;
        public static string[] endPhrases = new string[] { "ой все", "все", "конец", "заканчивай", "хорош", "пока" };

        /// <summary>
        /// конструктор по умолчанию, создает пустой список синапсов
        /// </summary>
        public Talk()
        {
            Sinapses = new List<Sinaps>();
        }
        /// <summary>
        /// конструктор от имени XML-файла
        /// </summary>
        /// <param name="FileName">имя файла</param>
        public Talk(string FileName)
        {
            Sinapses = LoadXml<List<Sinaps>>(FileName);
        }
        /// <summary>
        /// обучение с текстового файла
        /// </summary>
        /// <param name="FileName">имя файла</param>
        public void StudyFromFile(string FileName)
        {
            string s, s1 = "", s2 = "";
            //List<string> l = new List<string>();
            Encoding enc = Encoding.GetEncoding(1251);
            StreamReader st = new StreamReader(FileName, enc);
            int k = 0;
            while ((s = st.ReadLine()) != null)
            {
                //Console.WriteLine("{0} - считывание в цикле", s);
                s2 = s.Substring(3);
                s = st.ReadLine();
                //Console.WriteLine("{0} - считывание пустой строки", s);
                if (k == 0)
                {
                    s1 += s2;
                    s2 = st.ReadLine().Substring(3);
                    //Console.WriteLine("{0} - считывание второй строки, если это начало", s2);
                    s = st.ReadLine();
                    //Console.WriteLine("{0} - считывание пустой строки", s);
                    k++;
                }
                AddToList(s1, s2);
                s1 = s2;
            }
        }
        /// <summary>
        /// добавление пары реплик в список (если было - не добавляет)
        /// </summary>
        /// <param name="s1">первая реплика</param>
        /// <param name="s2">вторая реплика</param>
        public void AddToList(string s1, string s2)
        {
            int num = -1;
            int k = -1;
            Sinaps dr = new Sinaps(s1);
            for (int i = 0; i < Sinapses.Count; i++)
            {
                if (Sinapses[i].Content == dr.Content)
                {
                    num = i;
                    break;
                }
            }
            /*Sinaps dr = new Sinaps(s1);
            for (int i = 0; i < Sinapses.Count; i++)
                if (Sinapses[i].IsSame(dr))
                    num = i;*/
            if (s1 == "")
            {
                Console.ForegroundColor = ConsoleColor.Green;
                MessageBox.Show("строка пустая");
                Console.WriteLine("строка пуская!!!");
                Console.ForegroundColor = ConsoleColor.White;
                num = -2;
            }
            Sinaps sin = new Sinaps(s2);
            for (int i = 0; i < Sinapses.Count; i++)
            {
                if (Sinapses[i].Content == sin.Content)
                {
                    k = i;
                    if ((num != -1) && (num != -2))
                    {
                        Sinapses[num].AddAnswer(k); //добавляет к существующему ответ
                        break;
                    }
                    if (num == -1)
                    {
                        Sinapses.Add(dr);
                        if (Sinapses[Sinapses.Count - 1].Content != dr.Content)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            MessageBox.Show("ты дурак");
                            //Console.WriteLine("ты дурак");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        Sinapses[Sinapses.Count - 1].AddAnswer(i);
                    }
                    break;
                }
            }
            if (k == -1)
            {
                Sinapses.Add(sin);
                if (num > -1)
                {
                    Sinapses[num].AddAnswer(Sinapses.Count - 1);
                }
                if (num == -1)
                {
                    Sinapses.Add(dr);
                    Sinapses[Sinapses.Count - 1].AddAnswer(Sinapses.Count - 2);
                }
            }
        }
        /// <summary>
        /// удаляет выбранную строку и все, что с ней связано
        /// </summary>
        /// <param name="s"></param>
        public void Delete(string s)
        {
            for (int i = 0; i < Sinapses.Count; i++)
                if (Sinapses[i].Content == s)
                {
                    for (int j = 0; j < Sinapses.Count; j++)
                        for (int k = 0; k < Sinapses[j].Answers.Count; k++)
                            if (Sinapses[j].Answers[k] == i)
                                Sinapses[j].Answers.RemoveAt(k);
                    Sinapses[i].Answers.Clear();
                    break;
                }
        }

        /// <summary>
        /// просто составляет словарь из одних и тех же слов
        /// </summary>
        public static void MakeSlovar()
        {
            Slovar = new List<List<string>>();
            List<string> H = new List<string>();
            H.Add("Привет!");
            H.Add("Здравствуйте!");
            H.Add("Приветствую!");
            H.Add("Салют!");
            H.Add("Здарова");
            H.Add("Хай");
            Slovar.Add(H); //0 - приветствия
            List<string> M = new List<string>();
            M.Clear();
            M.Add("ладно.");
            M.Add("хорошо.");
            M.Add("ясно.");
            M.Add("понятно.");
            M.Add("продолжай.");
            M.Add("расскажи что-нибудь еще.");
            M.Add("окей.");
            M.Add("ну вот.");
            M.Add("как скажешь.");
            M.Add("с тобой интересно.");
            M.Add("о, интересно.");
            M.Add("прекрасно!");
            M.Add("чудесно.");
            M.Add("даже так?!");
            M.Add("ничего себе!");
            M.Add("да ладно!");
            M.Add("да ты что!");
            M.Add("ты меня утомил.");
            M.Add("и что?");
            Slovar.Add(M); //1 - ответы на случай, когда не знаешь, что ответить
            List<string> G = new List<string>();
            G.Add("давай сменим тему?");
            G.Add("поговорим о другом?");
            G.Add("я вот что спросить хотел:");
            G.Add("слушай, я спрошу, пока не забыл?");
            G.Add("мне не нравится эта тема. давай я кое-что спрошу?");
            G.Add("о другом поговорить не хочешь?");
            Slovar.Add(G); //2 - продолжение этого ответа
            List<string> D = new List<string>();
            D.Add("как тебе погода на улице?");
            D.Add("что ты думаешь о погоде сегодня?");
            D.Add("тебе нравится шоколадное мороженое?");
            //D.Add("как ты относишься к ?");
            D.Add("какой твой любимый цвет?");
            D.Add("тебе нравится солнечная погода?");
            D.Add("что ты делаешь в свободное время?");
            D.Add("какие книги ты любишь читать?");
            D.Add("у тебя есть либимый фильм");
            D.Add("я вот мороженое люблю. а какое блюдо тебе нравится?");
            //D.Add("не найдтся минуты поговорить о боге?");
            Slovar.Add(D); //3 - завершение этого ответа
        }
        /// <summary>
        /// делает предложение на случай, если ответить нечего
        /// </summary>
        /// <returns>предложение</returns>
        private string MakeSentense()
        {
            string s = "";
            Random ran = new Random();
            for (int i = 1; i <= 3; i++)
                s += Slovar[i][ran.Next(Slovar[i].Count)] + " ";
            return s.Substring(0, s.Length - 1);
        }
        /// <summary>
        /// возвращает рандомный синапс
        /// </summary>
        /// <returns></returns>
        public Sinaps GetRandomSinaps()
        {
            return Sinapses[new Random().Next(Sinapses.Count)];
        }
        /// <summary>
        /// выводит на экран что-то подобное XML-файлу
        /// </summary>
        public void Show()
        {
            for (int i = 0; i < Sinapses.Count; i++)
            {
                Console.WriteLine("{0} - {1}", i, Sinapses[i].Content);
                for (int k = 0; k < Sinapses[i].Answers.Count; k++)
                    Console.WriteLine("   {0}", Sinapses[i].Answers[k]);
            }
        }
        /// <summary>
        /// придумывает, что ответить
        /// </summary>
        /// <param name="previous">предыдущая реплика</param>
        /// <param name="question">последняя реплика</param>
        /// <returns>что ответить</returns>
        public string GetAnswer(string previous, string question)
        {
            int num = -1;
            int k = -1;
            Sinaps dr = new Sinaps(previous);
            /*float max1 = 0.25F;
            for (int i = 0; i < Sinapses.Count; i++)
            {
                if (SameSinaps.CoeffOfSame(Sinapses[i].Content, dr.Content) > max1)
                {
                    max1 = SameSinaps.CoeffOfSame(Sinapses[i].Content, dr.Content);
                    num = i;
                }
            }*/
            for (int i = 0; i < Sinapses.Count; i++)
                if (Sinapses[i].Content == dr.Content)
                {
                    num = i;
                    break;
                }
            if (previous == "")
            {
                num = -1;
                Random ran = new Random();
                int w = ran.Next(Slovar[0].Count);
                Console.WriteLine("{0}", w);
                return Slovar[0][w];
            }

            Sinaps sin = new Sinaps(question);
            /*float max2 = 0.25F;
            for (int i = 0; i < Sinapses.Count; i++)
            {
                if (SameSinaps.CoeffOfSame(Sinapses[i].Content, sin.Content) > max2)
                {
                    max2 = SameSinaps.CoeffOfSame(Sinapses[i].Content, sin.Content);
                    k = i;
                }
            }*/
            for (int i = 0; i < Sinapses.Count; i++) //ищет полное совпадение, чтобы добавить его к предыдущему
                if (Sinapses[i].Content == sin.Content)
                {
                    if (num != -1) Sinapses[num].AddAnswer(i); //добавляет к предыдущему ответ
                    k = Sinapses[i].GetAnswer();
                    Console.WriteLine("(такой уже был!)");
                    if (k == -1)
                    {
                        Console.WriteLine("(такой уже был, но он пустой)");
                        continue; //нечего ответить
                    }
                    return Sinapses[k].Content; //то отвечает
                }
            //если нет такого еще
            Sinapses.Add(new Sinaps(question));
            if (num != -1)
                Sinapses[num].AddAnswer(Sinapses.Count - 1);
            float max = 0.0F;
            int r = -1;
            for (int i = 0; i < Sinapses.Count; i++)
                if ((SameSinaps.CoeffOfSame(Sinapses[i].Content, sin.Content) > max) && (Sinapses[i].Answers.Count > 0))
                {
                    max = SameSinaps.CoeffOfSame(Sinapses[i].Content, sin.Content);
                    r = i;
                }
            if (max < 0.25F)
            {
                //составлем предложение
                Console.WriteLine("(не нашел совпадений)");
                return MakeSentense();
            }
            else
            {
                //возвращаем однин из возможный ответов
                int t = Sinapses[r].GetAnswer();
                //Console.WriteLine("{0}, {1}, {2}", t, Sinapses.Count, Sinapses[r].Content);
                return Sinapses[t].Content;
            }
        }
        /// <summary>
        /// придумывает, что ответить. Либо с обучением либо без
        /// </summary>
        /// <param name="previous">предыдущая реплика</param>
        /// <param name="question">последняя реплика</param>
        /// <param name="study">обучать ли</param>
        /// <returns>ответ</returns>
        public string GetAnswer(string previous, string question, bool study)
        {
            int num = -1;
            int k = -1;
            Sinaps dr = new Sinaps(previous);
            for (int i = 0; i < Sinapses.Count; i++)
                if (Sinapses[i].Content == dr.Content)
                {
                    num = i;
                    break;
                }
            if (previous == "") //если это первая фраза в предложении
            {
                num = -1;
                Random ran = new Random();
                int w = ran.Next(Slovar[0].Count);
                Console.WriteLine("{0}", w);
                return Slovar[0][w];
            }

            Sinaps sin = new Sinaps(question);
            for (int i = 0; i < Sinapses.Count; i++) //ищет полное совпадение, чтобы добавить его к предыдущему
            {
                if (Sinapses[i].Content == sin.Content)
                {
                    if (num != -1 && study) Sinapses[num].AddAnswer(i); //добавляет к предыдущему ответ, если идет обучение
                    k = Sinapses[i].GetAnswer();
                    Console.WriteLine("(такой уже был!)");
                    if (k == -1)
                    {
                        Console.WriteLine("(такой уже был, но он пустой)");
                        continue; //нечего ответить
                    }
                    return Sinapses[k].Content; //то отвечает
                }
            }
            //если нет такого еще
            if (study)
            {
                Sinapses.Add(new Sinaps(question));
                if (num != -1)
                {
                    Sinapses[num].AddAnswer(Sinapses.Count - 1);
                }
            }
            //Console.WriteLine("(такого еще не было!)");
            float max = 0.0F;
            int r = -1;
            for (int i = 0; i < Sinapses.Count; i++)
                if ((SameSinaps.CoeffOfSame(Sinapses[i].Content, sin.Content) > max) && (Sinapses[i].Answers.Count > 0))
                {
                    max = SameSinaps.CoeffOfSame(Sinapses[i].Content, sin.Content);
                    r = i;
                }
            if (max < 0.25F)
            {
                //составлем предложение
                Console.WriteLine("(не нашел совпадений)");
                return MakeSentense();
            }
            else
            {
                //возвращаем однин из возможный ответов
                int t = Sinapses[r].GetAnswer();
                //Console.WriteLine("{0}, {1}, {2}", t, Sinapses.Count, Sinapses[r].Content);
                return Sinapses[t].Content;
            }
        }
        /// <summary>
        /// сохраняет в XML-файле текущий список синапсов
        /// </summary>
        /// <param name="FN">имя файла</param>
        public void Save(string FN)
        {
            SaveXml(FN, Sinapses);
        }
        /// <summary>
        /// определяет, является ли строка окончанием и нужно прощаться
        /// </summary>
        /// <param name="s">строка</param>
        /// <returns>строка прощания?</returns>
        public static bool TheEndOfInput(string s)
        {
            s = s.Replace("ё", "е");
            s = SameSinaps.CleanInput(s.ToLower());
            for (int i = 0; i < endPhrases.Length; i++)
                if (SameSinaps.CoeffOfSame(endPhrases[i], s) > 0.8)
                    return true;
            return false;
        }
        /// <summary>
        /// сохранение в XML-файл
        /// </summary>
        /// <typeparam name="T">то, что сохраняем</typeparam>
        /// <param name="fileName">имя XML-файла</param>
        /// <param name="variable">имя того, что сохраняем</param>
        public void SaveXml<T>(string fileName, T variable)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new StreamWriter(fileName))
                serializer.Serialize(stream, variable);
        }
        /// <summary>
        /// загружает что-то из XML-файла
        /// </summary>
        /// <typeparam name="T">что-то</typeparam>
        /// <param name="fileName">имя файла</param>
        /// <returns>что-то(список синапсов)</returns>
        public T LoadXml<T>(string fileName)
        {
            T temp;
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new StreamReader(fileName))
                temp = (T)serializer.Deserialize(stream);
            return temp;
        }
    }
}
