﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ChatAI
{
    /// <summary>
    /// Класс для обработки похожих синапсов
    /// </summary>
    class SameSinaps
    {
        public static float MaxCommon = 0.6F;
        public static int LengthDifference = 4;

        //TODO: сравнивать строки метриков Левенштейна
        /// <summary>
        /// Сверка строк на равенство
        /// </summary>
        /// <param name="s1">первая строка</param>
        /// <param name="s2">вторая строка</param>
        /// <returns>равны ли строки</returns>
        public static bool HaveOriginal(string s1, string s2)
        {
            s1 = s1.Replace("ё", "е");
            return (s1 == s2);
        }
        /// <summary>
        /// определяет коэффициент похожести двух строк
        /// </summary>
        /// <param name="s1">первая строка</param>
        /// <param name="t1">вторая строка</param>
        /// <returns>коэффициент похожести</returns>
        public static float CoeffOfSame(string s1, string t1)
        {
            if (HaveOriginal(s1, t1))
                return 1.0F;
            float coeff = 0;
            float c = 0;
            int ls, lt;
            string[] s = IntoWordsArray(s1);
            string[] t = IntoWordsArray(t1);
            ls = s.Length;
            lt = t.Length;
            for (int i = 0; i < ls; i++)
            {
                for (int k = 0; k < lt; k++)
                {
                    if (SameWords1(s[i], t[k]))
                    {
                        //Console.WriteLine("1");
                        c++;
                    }
                    //там важная функция, включить её потом обратно!!!
                    /*else if (SameWords2(s[i], t[k]))
                    {
                        Console.WriteLine("2");
                        c++;
                    }*/
                    else if (SameWords3(s[i], t[k]))
                    {
                        Console.WriteLine("3");
                        c++;
                    }
                }
            }
            coeff = (float)c / ((ls + lt) / 2);
            //Console.WriteLine("{0}: {1} - коэффициент, {2} - слов похожих, {3} - в среднем слов", t1, coeff, c, ls + lt);
            return coeff;
        }
        
        
        /// <summary>
        /// определяет размер наибольшей общей подстроки (моя кривая версия)
        /// </summary>
        /// <param name="s">первая строка</param>
        /// <param name="F">вторая строка</param>
        /// <returns>размер наибольшей общей подстроки</returns>
        public static int SizeOfMaxSubString(string s, string F)
        {
            int len = 0; //длина подстроки
            int t = 0; //текущее значение
            //string str; //текущая подстрока
            int ind = 0;
            for (int i = 0; i < F.Length; i++)
            {
                for (int j = 0; j < s.Length; j++)
                {
                    while ((i + ind < F.Length) && (j + ind < s.Length) && (F[i + ind] == s[j + ind]))
                    {
                        t++;
                        ind++;
                    }
                    if (len < t)
                    {
                        len = t;
                    }
                    t = 0;
                    ind = 0;
                }
            }
            return len;
        }

        /// <summary>
        /// находит наибольшую общую подстроку и меняет аргумент на её длину
        /// </summary>
        /// <param name="s1">строка 1</param>
        /// <param name="s2">строка 2</param>
        /// <param name="length">длина</param>
        /// <returns>наибольшая общая подстрока</returns>
        public static string MaxSubString(string s1, string s2, int length)
        {
            int[,] a = new int[s1.Length + 1, s2.Length + 1];
            int u = 0, v = 0;

            for (var i = 0; i < s1.Length; i++)
            {
                for (var j = 0; j < s2.Length; j++)
                {
                    if (s1[i] == s2[j])
                    {
                        a[i + 1, j + 1] = a[i, j] + 1;
                        if (a[i + 1, j + 1] > a[u, v])
                        {
                            u = i + 1;
                            v = j + 1;
                        }
                    }
                }
            }
            length = a[u, v];
            return s1.Substring(u - a[u, v], a[u, v]);
        }
        /// <summary>
        /// вырезает из строк их наибольшую общую подстроку
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        public static void CutMaxSubString(string s1, string s2, int ind1, int ind2)
        {
            int[,] a = new int[s1.Length + 1, s2.Length + 1];
            int u = 0, v = 0;

            for (var i = 0; i < s1.Length; i++)
                for (var j = 0; j < s2.Length; j++)
                    if (s1[i] == s2[j])
                    {
                        a[i + 1, j + 1] = a[i, j] + 1;
                        if (a[i + 1, j + 1] > a[u, v])
                        {
                            u = i + 1;
                            v = j + 1;
                        }
                    }
            ind1 = u;
            ind2 = v;
            s1 = s1.Remove(u, a[u, v]);
            s2 = s2.Remove(v, a[u, v]);
        }

        //TODO: дописать так, чтобы он отбрасывал соседние к крайним буквам и проверял снова
        /// <summary>
        /// проверяет схожесть по длинне максимальной общей подстроки
        /// </summary>
        /// <param name="s">первое слово</param>
        /// <param name="F">второе слово</param>
        /// <returns>похожи ли эти слова</returns>
        public static bool SameWords1(string s, string F)
        {
            if (HaveOriginal(s, F))
                return true;
            if (Math.Abs(F.Length - s.Length) > LengthDifference)
                return false;
            string sub;
            int len = 0;
            sub = MaxSubString(s, F, len);
            float f = (float)len / ((float)(s.Length + F.Length) / 2);
            if (f >= 0.6)
                return true;
            else
                return false;
        }

        /*public static bool SameWords2(string s1, string s2)
        {
            int l1 = s1.Length;
            int l2 = s2.Length;
            int ind11 = 0;
            int ind12 = 0;
            int ind21 = 0;
            int ind22 = 0;
            SameSinaps.CutMaxSubString(s1, s2, ind11, ind12);
            SameSinaps.CutMaxSubString(s1, s2, ind21, ind22);
            if (((ind11 <= ind21 && ind12 <= ind22) || (ind11 > ind21 && ind12 > ind22)) && (((s1.Length + s2.Length) < 3)))
            {
                return true;
            }
            return false;
        }*/
        /// <summary>
        /// меняет раскладку первого слова и сравнивает со вторым
        /// </summary>
        /// <param name="s1">строка изменяемая</param>
        /// <param name="s2">строка, с которой сравниваюь</param>
        /// <returns>равны ли?</returns>
        public static bool SameWords3(string s, string t)
        {
            s = s.Replace("q", "й");
            s = s.Replace("w", "ц");
            s = s.Replace("e", "у");
            s = s.Replace("r", "к");
            s = s.Replace("t", "е");
            s = s.Replace("y", "н");
            s = s.Replace("u", "г");
            s = s.Replace("i", "ш");
            s = s.Replace("o", "щ");
            s = s.Replace("p", "з");
            s = s.Replace("[", "х");
            s = s.Replace("]", "ъ");
            s = s.Replace("a", "ф");
            s = s.Replace("s", "ы");
            s = s.Replace("d", "в");
            s = s.Replace("f", "а");
            s = s.Replace("g", "п");
            s = s.Replace("h", "р");
            s = s.Replace("j", "о");
            s = s.Replace("k", "л");
            s = s.Replace("l", "д");
            s = s.Replace(";", "ж");
            s = s.Replace("'", "э");
            s = s.Replace("z", "я");
            s = s.Replace("x", "ч");
            s = s.Replace("c", "с");
            s = s.Replace("v", "м");
            s = s.Replace("b", "и");
            s = s.Replace("n", "т");
            s = s.Replace("m", "ь");
            s = s.Replace(",", "б");
            s = s.Replace(".", "ю");
            s = s.Replace("/", ".");
            /*s = s.Replace("й", "q");
            s = s.Replace("ц", "w");
            s = s.Replace("у", "e");
            s = s.Replace("к", "r");
            s = s.Replace("е", "t");
            s = s.Replace("н", "y");
            s = s.Replace("г", "u");
            s = s.Replace("ш", "i");
            s = s.Replace("щ", "o");
            s = s.Replace("з", "p");
            s = s.Replace("х", "[");
            s = s.Replace("ъ", "]");
            s = s.Replace("ф", "a");
            s = s.Replace("ы", "s");
            s = s.Replace("в", "d");
            s = s.Replace("а", "f");
            s = s.Replace("п", "g");
            s = s.Replace("р", "h");
            s = s.Replace("о", "j");
            s = s.Replace("л", "k");
            s = s.Replace("д", "l");
            s = s.Replace("ж", ";");
            s = s.Replace("э", "'");
            s = s.Replace("я", "z");
            s = s.Replace("ч", "x");
            s = s.Replace("с", "c");
            s = s.Replace("м", "v");
            s = s.Replace("и", "b");
            s = s.Replace("т", "n");
            s = s.Replace("ь", "m");
            s = s.Replace("б", ",");
            s = s.Replace("ю", ".");*/
            return SameWords1(s, t);
        }
        /// <summary>
        /// Преобразовывает строку в массив слов.
        /// </summary>
        /// <param name="inputString">входящая строка</param>
        /// <returns>массив элементов, разделенных по пробелам и переведенных в нижний регистр</returns>
        private static string[] IntoWordsArray(string inputString)
        {
            //trim - чистит начало и конец от пробелов
            //split - возвращает массив разделов по ' '
            var result = inputString.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < result.Length; i++)
                result[i] = CleanInput(result[i].ToLower());
            return result;
        }

        /// <summary>
        /// Чистка строки от ненужных символов
        /// </summary>
        public static string CleanInput(string inputString)
        {
            return Regex.Replace(inputString, @"[^a-zа-яA-ZА-Я0-9 $]", "");
        }
    }
}
