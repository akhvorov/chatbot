﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ChatAI
{
    [Serializable]
    [XmlRoot("sinaps")]
    public class Sinaps
    {
        [XmlElement("content")]
        public string Content;
        [XmlArray("answers")]
        [XmlArrayItem("answer")]
        public List<int> Answers;
        /// <summary>
        /// создает пустой синапс, не инициализируя строку
        /// </summary>
        public Sinaps()
        {
            Answers = new List<int>();
        }
        /// <summary>
        /// Создает синапс с контентом и одним вариантом ответа на него
        /// </summary>
        /// <param name="content">строка контента</param>
        /// <param name="answer">вариант ответа</param>
        public Sinaps(string content, int answer)
        {
            Content = content;
            Answers = new List<int> { answer };
        }
        /// <summary>
        /// создает синапс с строкой
        /// </summary>
        /// <param name="content">строка контента</param>
        public Sinaps(string content)
        {
            Content = content;
            Answers = new List<int>();
        }
        /// <summary>
        /// добавляет в синапс номер нового варианта ответа, если его там еще не было
        /// </summary>
        /// <param name="num">номер ответа</param>
        public void AddAnswer(int num)
        {
            for (int i = 0; i < Answers.Count; i++)
                if (Answers[i] == num)
                    return;
            Answers.Add(num);
        }
        /// <summary>
        /// выдает один из ответов этого синапса
        /// </summary>
        /// <returns>номер ответа или -1 если список ответов пуст</returns>
        public int GetAnswer()
        {
            var r = new Random();
            int t = -1;
            if (Answers.Count != 0)
                t = Answers[r.Next(Answers.Count)];
            return t /*((Answers.Count == 0) ? (Answers[(r.Next(Answers.Count))]) : (-1))*/;
        }
        /// <summary>
        /// определяет, схожи ли два синапса. Коэффициент 0.5
        /// </summary>
        /// <param name="sin">Синапс, с которым сравнивать</param>
        /// <returns>похожи ли</returns>
        public bool IsSame(Sinaps sin)
        {
            if (SameSinaps.CoeffOfSame(this.Content, sin.Content) > 0.5)
                return true;
            return false;
        }
    }
}
